package com.test.rosbank.presentation.presenter

import com.arellomobile.mvp.InjectViewState
import com.arellomobile.mvp.MvpPresenter
import com.spacesofting.weshare.mvp.CurrencyData
import com.test.rosbank.api.Api
import io.reactivex.android.schedulers.AndroidSchedulers
import com.test.rosbank.presentation.view.CurrencyView
import io.reactivex.schedulers.Schedulers

@InjectViewState
class CurrencyPresenter : MvpPresenter<CurrencyView>() {
    fun getCurrent() {
        viewState.showProgress(true)
        Api.Currency.getCurrency()
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({ it ->
                    viewState.showProgress(false)
                viewState.show(it,"0")
            }, {
                it.printStackTrace()
                   viewState.showProgress(false)
            })
    }

    fun check(currencyData: CurrencyData, password: String) {
        viewState.show(currencyData, password)
    }
}
