package com.test.rosbank.presentation.view

import com.arellomobile.mvp.MvpView
import com.spacesofting.weshare.mvp.CurrencyData

interface CurrencyView : MvpView {
    fun show(it: CurrencyData, i: String)
    fun showProgress(isVisible: Boolean)
}
