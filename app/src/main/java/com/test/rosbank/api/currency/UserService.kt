package com.spacesofting.weshare.api.user


import com.spacesofting.weshare.mvp.CurrencyData
import retrofit2.http.*
import io.reactivex.Observable


interface CurrencyInterface {
    @GET("daily_json.js")
    fun getCurrency(): Observable<CurrencyData>
}