package com.test.rosbank.ui.activity

import android.os.Bundle
import android.text.SpannableStringBuilder
import android.widget.Toast
import androidx.recyclerview.widget.LinearLayoutManager
import com.arellomobile.mvp.MvpActivity
import com.arellomobile.mvp.presenter.InjectPresenter
import com.jakewharton.rxbinding.widget.RxTextView
import com.spacesofting.weshare.mvp.CurrencyData
import com.test.rosbank.presentation.presenter.CurrencyPresenter
import com.test.rosbank.presentation.view.CurrencyView
import com.test.rosbank.ui.adapter.CurrencyAdapter
import io.reactivex.subjects.PublishSubject
import kotlinx.android.synthetic.main.activity_main.*
import rx.android.schedulers.AndroidSchedulers
import java.util.concurrent.TimeUnit
import java.util.regex.Pattern
import android.app.Activity
import android.view.View
import android.view.inputmethod.InputMethodManager
import com.test.rosbank.R
import rx.Observable
import rx.schedulers.Schedulers
import rx.subscriptions.Subscriptions

class MainActivity : MvpActivity() , CurrencyView {

    @InjectPresenter
    lateinit var mCurrencyPresenter: CurrencyPresenter
    var adapter: CurrencyAdapter? = null
    val publish: PublishSubject<String> = PublishSubject.create()
    var currencyData = CurrencyData()
    var subscription = Subscriptions.unsubscribed()

    override fun onCreate(savedInstanceState: Bundle?)
    {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        mCurrencyPresenter.getCurrent()
        subscription = Observable.interval(60, TimeUnit.SECONDS)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe {
                mCurrencyPresenter.getCurrent()
            }

        RxTextView.textChanges(calculationInPut)
            .map(CharSequence::toString)
            .debounce(1000, TimeUnit.MILLISECONDS)
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe { numbers ->
                publish.onNext(numbers.toString())
                if (countDots(numbers) || numbers.startsWith("00")) {
                    calculationInPut.text = SpannableStringBuilder(numbers.substring(0, numbers.length - 1))
                    Toast.makeText(
                        this@MainActivity,
                        getString(R.string.set_point_text),
                        Toast.LENGTH_LONG
                    ).show()
                } else {
                    if (validinput(numbers)) {
                        numbers?.let {
                            mCurrencyPresenter.check(
                                currencyData,
                                numbers
                            )
                        }
                        hideKeyboard(this)
                    }
                }
            }
        inPutClear.setOnClickListener { calculationInPut.setText(null) }
    }

    override fun onStop() {
        super.onStop()
        subscription.unsubscribe()
    }
    override fun show(it: CurrencyData, index: String) {
        val editavleText = calculationInPut.text.toString()
        var indexFold = index

        if (index.isNotEmpty() || index.equals("0") && editavleText.isNotEmpty()) {
            indexFold = editavleText
        }
        currencyData = it
        val cur = it.valute
        adapter = cur?.let { it1 -> CurrencyAdapter(it1, indexFold) }
        cyrrentShow.adapter = adapter
        cyrrentShow.layoutManager = LinearLayoutManager(this)
        adapter?.notifyDataSetChanged()
    }

    private fun validinput(input: String): Boolean {
        var finds: Boolean
        val p = Pattern.compile("^(\\d+)(\\.{0,1})?$")
        finds = p.matcher(input).find() || input.contains(".")
        return finds
    }

    fun countDots(str: String): Boolean {
        var ret = 0
        var test = false
        for (element in str.toCharArray()) {
            if (element.equals('.')) {
                ret++
            }
        }
        test = ret > 1
        return test
    }
    fun hideKeyboard(activity: Activity) {
        val imm = activity.getSystemService(Activity.INPUT_METHOD_SERVICE) as InputMethodManager
        //Find the currently focused view, so we can grab the correct window token from it.
        var view = activity.currentFocus
        //If no view currently has focus, create a new one, just so we can grab a window token from it
        if (view == null) {
            view = View(activity)
        }
        imm.hideSoftInputFromWindow(view.windowToken, 0)
    }
    override fun showProgress(isVisible: Boolean) {
        if (isVisible) {
            progressBar.visibility = View.VISIBLE
        } else {
            progressBar.visibility = View.GONE
        }
    }
}
