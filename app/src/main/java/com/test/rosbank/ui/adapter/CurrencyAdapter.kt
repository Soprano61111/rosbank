package com.test.rosbank.ui.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.spacesofting.weshare.mvp.CurrencyData
import com.spacesofting.weshare.mvp.Current
import com.test.rosbank.R
import kotlinx.android.synthetic.main.list_item_currency.view.*

class CurrencyAdapter(
    curr: HashMap<String,Current>,
    calculate: String) : RecyclerView.Adapter<CurrencyAdapter.CompilationViewHolder>() {
    val dataset  =  curr
    val amount = calculate
    var result: Float? = null

    override fun getItemCount(): Int = dataset.size

    override fun onBindViewHolder(holder: CompilationViewHolder, position: Int) {
        val entry = dataset.entries.elementAt(position)
        val key = entry.key
        holder.abbreviation.text = key
        val value = entry.value.value
        if (amount.isNotEmpty() && amount != "0") {
            result = value?.let { amount.toFloat() / it }
            holder.value.text = result.toString()
        } else {
            holder.value.text = value.toString()
        }
        holder.nameValute.text = entry.value.name
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CompilationViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.list_item_currency, parent, false)
        return CompilationViewHolder(view)
    }

    class CompilationViewHolder(item: View): RecyclerView.ViewHolder(item){
        var abbreviation = item.abbreviation
        var value = item.valute
        var nameValute = item.nameValute
    }
}