package com.spacesofting.weshare.mvp

import com.google.gson.annotations.SerializedName


class CurrencyData {
   @SerializedName("Date")
   var date: String? = null
   @SerializedName("PreviousDate")
   var previousDate: String? = null
   @SerializedName("PreviousURL")
   var previousURL: String? = null
   @SerializedName("Timestamp")
   var timestamp: String? = null
   @SerializedName("Valute")
   var valute: HashMap<String, Current> = HashMap()


}