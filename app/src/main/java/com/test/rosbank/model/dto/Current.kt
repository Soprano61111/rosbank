package com.spacesofting.weshare.mvp

import com.google.gson.annotations.SerializedName
import java.io.Serializable
import com.google.gson.annotations.Expose


open class Current {
   @SerializedName("ID")
   var iD: String? = null
   @SerializedName("NumCode")
   var numCode: String? = null
   @SerializedName("CharCode")
   var charCode: String? = null
   @SerializedName("Nominal")
   var nominal: Int = 0
   @SerializedName("Name")
   var name: String? = null
   @SerializedName("Value")
   var value: Float? = null
   @SerializedName("Previous")
   var previous: Float? = null

   override fun equals(other: Any?): Boolean {
      if (this === other) {
         return true
      }

      if (other == null || other.javaClass != this.javaClass) {
         return false
      }

      if (other is Current) {
         if (iD == other.iD &&
            numCode == other.numCode &&
            charCode == other.charCode &&
            nominal == other.nominal &&
            name == other.name &&
            value == other.value &&
            previous == other.previous) {
            return true
         }
      }
      return false
   }

   override fun hashCode(): Int {
      val prime = 31
      var result = nominal
      result = prime * result + (iD?.hashCode() ?: 0)
      result = prime * result + (numCode?.hashCode() ?: 0)
      result = prime * result + (charCode?.hashCode() ?: 0)
      result = prime * result + (name?.hashCode() ?: 0)
      result = prime * result + (value?.hashCode() ?: 0)
      result = prime * result + (previous?.hashCode() ?: 0)

      return result
   }
}